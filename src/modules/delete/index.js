// Imports required by the application
import React from 'react';
import styled from 'styled-components';
import { Col, Form, Image, Button } from 'react-bootstrap';

/**
 * Form module to delete the products
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
export default function Delete({ item, handleClose }) {
  const ContainerButtons = styled.div`
    margin-top: 20px;
  `;

  /**
   * Function for when if you want to remove the product
   * @param {*} item - Product to be removed
   */
  const handleSave = (item) => handleClose({ data: item });

  return (
    <>

      <Col xs={12} md={12} style={{ display: 'flex', justifyContent: 'center' }}>
        <Image
          style={{ width: '200px', height: '200px' }}
          src="https://t3.ftcdn.net/jpg/02/69/66/02/240_F_269660263_wICqesonYg9VCcNiVnFzEhmuTWCyeu4Q.jpg"
        />
      </Col>

      <Form.Label column sm="12" style={{ display: 'flex', justifyContent: 'center', color: '#ff0000', fontSize: '17px' }}>
        ¿Está seguro que desea eliminar el producto?
      </Form.Label>

      <Form.Label column sm="12" style={{ display: 'flex', justifyContent: 'center', fontWeight: 'bold' }}>
        {`${item.id} -> ${item.name}`}
      </Form.Label>

      <ContainerButtons>

        <Button variant="primary" style={{ float: 'right', marginLeft: '10px' }} onClick={() => handleSave(item)}>
          Si, eliminar producto
        </Button>

        <Button variant="secondary" style={{ float: 'right' }} onClick={handleClose}>
          Cancelar
        </Button>

      </ContainerButtons>

    </>
  );
}
