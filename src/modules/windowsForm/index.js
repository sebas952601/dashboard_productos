// Imports required by the application
import React from 'react';
import styled from 'styled-components';
import { atom, useRecoilState } from 'recoil';
import { Form, Button } from 'react-bootstrap';

/**
 * Form module to add or edit the products
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 * @param     {*} {
 *  handleClose: Function to close the modal,
 *  item: In case of being to edit or delete, the selected item will arrive,
 *  lastId: If an insert is going to be made, the id of the last product in the list will arrive,
 * }
 */
export default function WindowsForm({ handleClose, item, lastId }) {
  /**
   * In this section we are going to initialize the state of the price, if it is an edition then we put the price that comes from the selected product, otherwise it is empty.
   * In the key if a new product is added, we take the id of the last product in the list plus one to create the unique key for that element to be created, otherwise the id of the selected product is put.
   * @type {*}
   */
  const priceState = atom({
    default:  item ? item.price : '',
    key:      `price${lastId ? `${lastId + 1}` : item.id}`,
  });

  /**
   * With this function we are going to update the status of price
   * @type {*}
   */
  const [price, setPrice] = useRecoilState(priceState);

  /**
   * Price format function
   * @param {*} event
   * @param {*} onChange
   */
  const onChangePrice = (event, onChange) => {
    const number  = `${!onChange ? event : event.target.value}`.match(/[\d]/g);
    const value   = !number ? 0 : Number(number.join(''));

    const VALUE = `${Math.round(value)}`;

    if (!onChange) return `$ ${VALUE.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
    else setPrice(`$ ${VALUE.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`);
  }

  /**
   * In this section we are going to initialize the state of the name, if it is an edition then we put the name that comes from the selected product, otherwise it is empty.
   * In the key if a new product is added, we take the id of the last product in the list plus one to create the unique key for that element to be created, otherwise the id of the selected product is put.
   * @type {*}
   */
  const nameState = atom({
    default:  item ? item.name : '',
    key:      `name${lastId ? `${lastId + 1}` : item.id}`,
  });

  /**
   * With this function we are going to update the status of name
   * @type {*}
   */
  const [name, setName] = useRecoilState(nameState);

  /**
   * In this section we are going to initialize the state of the description, if it is an edition then we put the description that comes from the selected product, otherwise it is empty.
   * In the key if a new product is added, we take the id of the last product in the list plus one to create the unique key for that element to be created, otherwise the id of the selected product is put.
   * @type {*}
   */
  const descriptiomState = atom({
    default:  item ? item.description : '',
    key:      `description${lastId ? `${lastId + 1}` : item.id}`,
  });

  /**
   * With this function we are going to update the status of the description
   * @type {*}
   */
  const [description, setDescription] = useRecoilState(descriptiomState);

  /**
   * In this section we are going to initialize the state of the category, if it is an edition then we put the category that comes from the selected product, otherwise it is empty.
   * In the key if a new product is added, we take the id of the last product in the list plus one to create the unique key for that element to be created, otherwise the id of the selected product is put.
   * @type {*}
   */
  const categoryState = atom({
    default:  item ? item.category : '',
    key:      `category${lastId ? `${lastId + 1}` : item.id}`,
  });

  /**
   * With this function we are going to update the status of category
   * @type {*}
   */
  const [category, setCategory] = useRecoilState(categoryState);

  // When the information of the form has already been configured, an object is created with the configured data and it is sent to the function in charge of creating/editing the data and closing the modal
  const handleSave = () => {
    const data = {
      name,
      price,
      category,
      description,
      dateSale: '',
      id:       item ? item.id : `${lastId + 1}`,
    };

    handleClose({ data });
  }

  const ContainerButtons = styled.div`
    margin-top: 20px;
  `;

  const { Group, Label, Control: Input, Select } = Form;
  return (
    <>

      <Group className="mb-3" controlId="name">
        <Label>Nombre</Label>
        <Input type="string" value={name} placeholder="Ejemplo: Hamburguesa" onChange={(event) => setName(event.target.value)} />
      </Group>

      <Group className="mb-3" controlId="price">
        <Label>Precio</Label>
        <Input type='string' value={onChangePrice(price, false)} placeholder="$ 230.333.00" onChange={(event) => onChangePrice(event, true)} maxLength={15} />
      </Group>

      <Group className="mb-3" controlId="desription">
        <Label>Descripción</Label>
        <Form.Control as="textarea" rows={3} value={description} placeholder="Ejemplo: Hamburguesa con pan artesanal y queso parmesano" onChange={(event) => setDescription(event.target.value)} />
      </Group>

      <Group className="mb-3" controlId="categoria">
        <Label>Categoría</Label>
        <Select aria-label="Default select example" onChange={(event) => setCategory(event.target.value)} value={category}>
          <option>Seleccione una categoria</option>
          <option value="0">Comida</option>
          <option value="1">Vestuario</option>
          <option value="2">Calzado</option>
          <option value="3">Lenceria</option>
          <option value="4">Electrodoméstico</option>
        </Select>
      </Group>

      <ContainerButtons>

        <Button variant="primary" onClick={handleSave} style={{ float: 'right', marginLeft: '10px' }}>
          Guardar cambios
        </Button>

        <Button variant="secondary" onClick={handleClose} style={{ float: 'right' }}>
          Cancelar
        </Button>
        
      </ContainerButtons>

    </>
  );
}
