// Imports required by the application
import React from 'react';
import styled from 'styled-components';

/**
 * Header component function
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-20
 * @author    Sebastian Saldarriaga
 */
export default function CmpsHeader({ text }) {
  const Header = styled.h1`
    align-items: center;
    background-color: #ddcaba;
    display: flex;
    height: 40px;
    justify-content: center;
    position: relative;
    width: 100%;
  `;

  return <Header>{text}</Header>;
}
