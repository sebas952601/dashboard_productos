import { Image, OverlayTrigger, Tooltip } from "react-bootstrap";

export default function CmpsImage({ tootlipText, onClick, styles, src }) {
  let image = <Image rounded onClick={onClick} style={styles} src={src} />;

  if (tootlipText) {
    image = (
      <OverlayTrigger
        key="delete"
        placement="bottom"
        overlay={
          <Tooltip id={src}>{tootlipText}</Tooltip>
        }
      >
        {image}
      </OverlayTrigger>
    )
  }

  return image;
}