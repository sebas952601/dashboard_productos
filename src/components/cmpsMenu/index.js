// Imports required by the application
import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Col, Image } from 'react-bootstrap';
import CmpsImage from '../cmpsImage';

/**
 * Menu component function
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-20
 * @author    Sebastian Saldarriaga
 */
export default function CmpsMenu({ handleShow }) {
  const ContainerMenu = styled.div`
    background-color: #678293;
    left: 0;
    height: 100%;
    position: fixed;
    top: 0;
    width: 80px;
  `;

  const Menu = styled.div`
    margin-top: 140px;
  `;

  return (
    <ContainerMenu>

      <Col xs={1} md={1}>
        <Link to="/">
          <CmpsImage 
            src="https://cdn-icons-png.flaticon.com/512/1493/1493681.png"
            styles={{ width: '70px', height: '70px', margin: '10px 0px 0px 5px' }}
          />
        </Link>
      </Col>

      <Menu>

        <Col xs={1} md={1}>
          <CmpsImage
            onClick={handleShow}
            tootlipText="Agregar producto"
            styles={{ width: '50px', height: '50px', marginLeft: '15px' }} 
            src="https://cdn-icons.flaticon.com/png/512/4347/premium/4347829.png?token=exp=1637423654~hmac=4eacfba55ab42ee59d48117ecf1d92e0"
          />
        </Col>

        {[
          { img: 'https://cdn-icons-png.flaticon.com/512/2979/2979720.png', to: '/', tooltip: 'Ver el listado de productos' },
          { img: 'https://cdn-icons-png.flaticon.com/512/4222/4222019.png', to: '/dashboard', tooltip: 'Visualizar dashboards' }
        ].map((item, index) => (
          <Col xs={1} md={1} key={index}>
            <Link to={item.to}>
              <CmpsImage src={item.img} styles={{ width: '50px', height: '50px', margin: '50px 0px 0px 15px' }} tootlipText={item.tooltip} />
            </Link>
          </Col>
        ))}

      </Menu>

    </ContainerMenu>
  );
}
