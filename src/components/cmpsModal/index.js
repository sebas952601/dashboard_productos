// Imports required by the application
import React from 'react';
import { Modal } from 'react-bootstrap';

/**
 * Modal component function
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-20
 * @author    Sebastian Saldarriaga
 */
export default function CmpsModal({ children, show, title }) {
  return (
    <Modal show={show} backdrop="static" keyboard={false}>

      {title && (
        <Modal.Header>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
      )}

      <Modal.Body>
        {children}
      </Modal.Body>

    </ Modal>
  );
}
