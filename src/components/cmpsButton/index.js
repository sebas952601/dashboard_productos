// Imports required by the application
import React from 'react';
import { Button } from 'react-bootstrap';

/**
 * Button component function
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-20
 * @author    Sebastian Saldarriaga
 */
export default function CmpsButton({ type = 'primary', label = '' }) {
  return <Button variant={type}>{label}</Button>;
}
