// Imports required by the application
import styled from 'styled-components';
import Products from './windows/products';
import { Route, Routes } from 'react-router';
import { atom, useRecoilState } from 'recoil';
import { BrowserRouter } from 'react-router-dom';
import './App.css';

// Functional component imports
import { arrProducts } from './data';
import Menu from './components/cmpsMenu';
import Dashboard from './windows/dashboard';
import CmpsModal from './components/cmpsModal';
import WindowsForm from './modules/windowsForm';

/**
 * Initial function of the application
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
function App() {
  /**
   * Variable to know the id of the last product found in the list
   * @type {*}
   */
  const lastId = arrProducts[arrProducts.length - 1].id;

  /**
   * In this section we are going to initialize the state of the modal, by default in false so that it is not shown
   * @type {*}
   */
  const modalState = atom({
    default:  false,
    key:      'modalAdd',
  });

  /**
   * With this function we are going to update the status of the modal (if it is going to be shown or not)
   * @type {*}
   */
  const [show, setShow] = useRecoilState(modalState);

  // Function that we use to display the modal
  const handleShowModal = () => setShow(true);

  /**
   * Function that we use to add a new product and to hide the modal.
   * @param {*} {
   *  data: Product to be added to the list
   * }
   */
  const handleHideModal = ({ data }) => {
    if (data) {
      data.id = `${Number(lastId) + 1}`;

      arrProducts.push(data);
    }

    setShow(false);
  }

  /**
   * We create the styles for the Content, which is the part where the different modules will be displayed (Products, Dashboard)
   * @type {*}
   */
  const Content = styled.div`
    background-color: #caedef;
    height: 100%;
    left: 80px;
    overflow: auto;
    position: relative;
    width: calc(100% - 80px);
  `;

  return (
    <BrowserRouter>
      <CmpsModal show={show} onHide={handleHideModal} title="Agregar producto">
          {/* As a new item is being created, we send it the id(lastId) of the last position plus one, to initialize the status of the modal components */}
          <WindowsForm handleClose={handleHideModal} lastId={lastId} />
      </ CmpsModal>

      <Menu handleShow={handleShowModal} />

      <Content>
        <Routes>
          <Route path="/" element={<Products arrProducts={arrProducts} />} />
          <Route path="dashboard" element={<Dashboard />} />
        </Routes>
      </Content>


    </BrowserRouter>
  );
}

export default App;
