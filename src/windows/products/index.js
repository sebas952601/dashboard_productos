// Imports required by the application
import React from 'react';
import styled from 'styled-components';
import { atom, useRecoilState } from 'recoil';
import { Table, Image, OverlayTrigger, Tooltip } from 'react-bootstrap';

// Functional component imports
import Delete from '../../modules/delete';
import CmpsModal from '../../components/cmpsModal';
import WindowsForm from '../../modules/windowsForm';
import CmpsHeader from '../../components/cmpsHeader';
import { arrCategories, arrProducts } from '../../data';
import CmpsImage from '../../components/cmpsImage';

/**
 * In this section we are going to initialize the state of the modal, by default in false so that it is not shown
 * @type {*}
 */
const modalState = atom({
  default:  false,
  key:      'modalUpd',
});

/**
 * In this section we are going to initialize the status of the item to update, by default it is an empty object
 * @type {*}
 */
const itemSelected = atom({
  default:  {},
  key:      'itemUpd',
});

/**
 * In this section we are going to initialize the state of the modal to delete, by default it is false so that it is not shown
 * @type {*}
 */
const showDelet = atom({
  default:  false,
  key:      'productDelet',
});

/**
 * Function to show the list of products
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
export default function Products() {
  /**
   * With this function we are going to update the status of the modal (if it is going to be shown or not)
   * @type {*} */
  const [showForm, setShowForm] = useRecoilState(modalState);

  /**
   * With this function we are going to update the status of the item, we save the selected item
   * @type {*} */
  const [item, setItem] = useRecoilState(itemSelected);

  /**
   * With this function we are going to update the status of the modal to delete (if it is going to be shown or not)
   * @type {*}
   */
  const [delet, setDelet] = useRecoilState(showDelet);

  /**
   * Function to update the item for which it was selected and open the module
   * @param {object} product - Product selected
   */
  const handleShowModal = (product) => {
    setItem(product);
    setShowForm(true);
  }

  /**
   * Function to send the edited information and/or close the module
   * @param {*} {
   *  data: Edited Information to midify in the array
   * }
   */
  const handleHideModal = ({ data }) => {
    if (data) {
      const positionProductEdit = arrProducts.findIndex(product => Number(product.id) === Number(data.id));
      arrProducts[positionProductEdit] = { ...data };
    }

    setShowForm(false);
  }

  /**
   * Function to update the item for which it was selected and open the modal to delete
   * @param {object} product - Product selected
   */
  const handleShowDelet = (product) => {
    setItem(product);
    setDelet(true);
  }

  /**
   * Function to send the edited information and/or close the module
   * @param {*} {
   *  data: Product to be removed
   * }
   */
  const handleHideDelet = ({ data }) => {
    if (data) {
      const positionProductDelet = arrProducts.findIndex(product => Number(product.id) === Number(item.id));
      arrProducts.splice(positionProductDelet, 1);
    }

    setDelet(false);
  }

  /**
   * Function to format the price
   * @param {string} value - Price of the product to be formatted
   */
  const onChangePrice = price => {
    const number      = price.match(/[\d]/g);
    const valueJoin   = !number ? 0 : Number(number.join(''));
    const valueRound  = `${Math.round(valueJoin)}`;

    return `$ ${valueRound.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
  };

  /**
   * The function to translate the category
   * @param {*} category - Product to which the category will be translated
   */
  const findNameCategory = (category) => {
    const objCategory = arrCategories.find(item => Number(item.id) === Number(category));
    return objCategory.name;
  }

  // We define the styles of the td component
  const Td = styled.td`
    display: table-cell;
    vertical-align: middle;
  `;

  // We define the styles of the div component
  const ContainerTable = styled.div`
    padding: 30px;
  `;

  return (
    <>

      <CmpsModal show={showForm} title="Editar producto">
        <WindowsForm handleClose={handleHideModal} item={item} />
      </ CmpsModal>

      <CmpsModal show={delet}>
        <Delete handleClose={handleHideDelet} item={item} />
      </ CmpsModal>

      <CmpsHeader text="Productos" />

      <ContainerTable>

        <Table striped bordered hover responsive="sm" style={{ padding: '30px' }}>
          <thead>
            <tr>
              <th style={{ width: '100px' }}>Acciones</th>
              <th>Nombre</th>
              <th style={{ width: '100px' }}>Precio</th>
              <th>Categoría</th>
              <th>Descripción</th>
            </tr>
          </thead>

          <tbody>
            {
              arrProducts.map((product, index) => (
                <tr key={index}>
                  <Td>
                    <CmpsImage
                      tootlipText="Editar producto"
                      onClick={() => handleShowModal(product)}
                      styles={{ width: '40px', height: '40px' }}
                      src="https://cdn-icons-png.flaticon.com/512/2271/2271629.png"
                    />
                    <CmpsImage
                      tootlipText="Eliminar producto"
                      onClick={() => handleShowDelet(product)}
                      styles={{ width: '40px', height: '40px' }}
                      src="https://cdn-icons.flaticon.com/png/512/5166/premium/5166939.png?token=exp=1637423789~hmac=285cfb37b0a529c374e0fe0404ed8457"
                    />
                  </Td>
                  <Td>{product.name}</Td>
                  <Td>{onChangePrice(product.price)}</Td>
                  <Td>{findNameCategory(product.category)}</Td>
                  <Td>{product.description}</Td>
                </tr>
              ))
            }
          </tbody>
        </Table>

      </ContainerTable>

    </>
  );
}