// Imports required by the application
import React from 'react';
import styled from 'styled-components';
import { Form, Table } from 'react-bootstrap';

/**
 * Function to display a table with earnings per month.
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
export default function ChartDonutEarningsPerMonth({ arrProducts }) {
  const arrCounterForMonth  = [];
  const counterForMonth     = { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0 };
  const arrMonths           = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

  // We will look for the products sold to add up the profits
  arrProducts.forEach(({ dateSale, price }) => {
    if (dateSale.trim() !== '') {
      const Navidad = new Date(dateSale);
      const mes = Navidad.getMonth();
      counterForMonth[mes] += Number(price);
    }
  });

  // We assemble the array that we are going to show in the graph
  for (const key in counterForMonth) {
    arrCounterForMonth.push({ month: arrMonths[key], price: counterForMonth[key] });
  }

  /**
   * Function to format the price
   * @param {string} value - Price of the product to be formatted
   */
  const onChangePrice = price => {
    const number = price.match(/[\d]/g);
    const value = !number ? 0 : Number(number.join(''));
    const VALUE = `${Math.round(value)}`;

    return `$ ${VALUE.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
  };

  const Container = styled.div`
    padding: 10px;
  `

  const Td = styled.td`
    display: table-cell;
    vertical-align: middle;
  `;

  return (
    <Container>

      <Form.Label column sm="12" style={{ display: 'flex', justifyContent: 'center', fontWeight: 'bold' }}>
        Total ganacias por mes
      </Form.Label>

      <Table striped bordered hover responsive="sm" style={{ padding: '30px' }}>
          <thead>
            <tr>
              <th>Mes</th>
              <th>Ganancia</th>
            </tr>
          </thead>
          <tbody>
            {
              arrCounterForMonth.map(({ month, price }, index) => (
                <tr key={index}>
                  <Td>{month}</Td>
                  <Td>{onChangePrice(`${price}`)}</Td>
                </tr>
              ))
            }
          </tbody>
        </Table>

    </Container>
  );
}
