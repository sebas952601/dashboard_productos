// Imports required by the application
import React from 'react';
import Chart from "react-apexcharts";
import { Form } from 'react-bootstrap';
import styled from 'styled-components';

/**
 * Function to display the bar graph.
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
export default function ChartBarMostSellectProducts({ arrProducts, arrCategories }) {
  const categories        = {};
  const nameCategories    = [];
  const valuesCategories  = [];

  // We are going to search from the categories that are in stock
  arrCategories.forEach(category => {
    if (!categories.hasOwnProperty(category.name)) categories[category.name] = 0;

    const filter_categories = arrProducts.filter(product => Number(product.category) === Number(category.id) && product.dateSale !== '');
    categories[category.name] = filter_categories.length;
  });

  // We put together an array to paint the information on the graph
  for (const key in categories) {
    nameCategories.push(key);
    valuesCategories.push(categories[key]);
  }

  const Container = styled.div`
    padding: 10px;
  `

  return (
    <Container>

      <Form.Label column sm="12" style={{ display: 'flex', justifyContent: 'center', fontWeight: 'bold' }}>
        Categorías mas vendidas
      </Form.Label>

      <Chart
        type="bar"
        width="550"
        series={
          [
            {
              name: 'Total vendidos',
              data: valuesCategories
            }
          ]
        }
        options={
          {
            chart: {
              id: "basic-bar"
            },
            xaxis: {
              categories: nameCategories
            }
          }
        }
      />
    </Container>
  );
}
