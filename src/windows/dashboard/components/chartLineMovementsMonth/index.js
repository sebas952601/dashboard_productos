// Imports required by the application
import React from 'react';
import Chart from "react-apexcharts";
import { Form } from 'react-bootstrap';
import styled from 'styled-components';

/**
 * Function to display the line graph.
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-21
 * @author    Sebastian Saldarriaga
 */
export default function ChartLineMovementsMonth({ arrProducts }) {
  const arrCounterForMonth  = [];
  const counterForMonth     = { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0 };

  // We will look for the products sold to add up the profits
  arrProducts.forEach(({ dateSale }) => {
    if (dateSale.trim() !== '') {
      const Navidad = new Date(dateSale);
      const mes = Navidad.getMonth();
      counterForMonth[mes] += 1;
    }
  });

  // We put together an array to paint the information on the graph
  for (const key in counterForMonth) {
    arrCounterForMonth.push(counterForMonth[key]);
  }

  const Container = styled.div`
    padding: 10px;
  `

  return (
    <Container>

      <Form.Label column sm="12" style={{ display: 'flex', justifyContent: 'center', fontWeight: 'bold' }}>
        Movimientos por mes
      </Form.Label>

      <Chart
        type="line"
        width="750"
        series={
          [
            {
              name: 'Total ventas',
              data: arrCounterForMonth,
            }
          ]
        }
        options={
          {
            chart: {
              id: "basic-bar"
            },
            xaxis: {
              categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
            }
          }
        }
      />
    </Container>
  );
}
