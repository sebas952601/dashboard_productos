// Imports required by the application
import React from 'react';
import styled from 'styled-components';

// Functional component imports
import Header from '../../components/cmpsHeader';
import { arrCategories, arrProducts } from '../../data';
import ChartBarCategoriesStock from './components/chartBarCategoriesStock';
import ChartLineMovementsMonth from './components/chartLineMovementsMonth';
import ChartDonutEarningsPerMonth from './components/chartDonutEarningsPerMonth';
import ChartBarMostSellectProducts from './components/chartBarMostSellectProducts';

/**
 * Function to show the dashboard
 * @export
 * @version   1.0.0
 * @date      2021-11-19
 * @last_date 2021-11-20
 * @author    Sebastian Saldarriaga
 */
export default function Dashboard() {
  const ContainerBars = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 50px;
  `;

  return (
    <>
      <Header text="Dashboard" />

      <ContainerBars>
        <ChartBarMostSellectProducts arrProducts={arrProducts} arrCategories={arrCategories} />
        <ChartBarCategoriesStock arrProducts={arrProducts} arrCategories={arrCategories} />
      </ContainerBars>

      <ContainerBars>
        <ChartLineMovementsMonth arrProducts={arrProducts} arrCategories={arrCategories} />
        <ChartDonutEarningsPerMonth arrProducts={arrProducts} />
      </ContainerBars>

    </>
  );
}
