# Mensajería Express

## Descripción

En este proyecto vamos a encontrar un listado de productos que hay en para una empresa de mensajeria, con su nombre, precio, categoria y descripción.

También vamos a encontrar unas opciones adicionales que son `Agregar producto` en el menú y también encontramos `Editar` y `Eliminar` por cada producto de la lista.

En el menú vamos a encontrar una opción donde vamos a poder visualizar unos graficos donde encontraremos diferentes movimientos realizados (Categorias mas vendida, Categorias en stock, etc), esto para un informe mas detallado de los movimientos que ha tenido la empresa con sus productos.

## Pasos de instalación

1. Abrir una consola y ubicarse en su lugar deseado. Ejemplos: `cd Desktop`, `cd Documents`, etc.
2. Ingresar el siguiente comando para clonar el proyecto `git clone https://gitlab.com/sebas952601/dashboard_productos.git`.
3. El siguiente paso es ingresar a nuestro proyecto `cd dashboard_productos`.
4. Ingresamos el siguiente comando para instalar todas la dependencias requeridas `npm i` o `npm install`.
5. Y por último ingresamos `npm start` para iniciar el proyecto.

## ¿Como navegar en la aplicación?

En el lado izquierdo se encuentra un Menú con el cual podremos realizar las siguientes acciones:

1. Agregar producto 
2. Ver el listado de productos
3. Visualizar dashboards

![menu](img/menu.PNG)

Al momento de cargar la aplicación entra a la visualización del listado de productos...

![list](img/productos.PNG)

Al dar clic en la primera opción `Agregar producto` nos muestra el siguiente modal

![add](img/agregar.PNG)

Al dar clic en la última opción `Visulizar dashboards` nos muestra una información detallada de los movimientos de la empresa

![dashboard1](img/dashboard1.PNG)
![dashboard2](img/dashboard2.PNG)

La visualización del listado de productos se muestra al dar clic en la segunda opción del menú. En esta sección hay una tabla donde cada fila tiene las acciones de `Editar producto` y `Eliminar producto`

![upd](img/editar.PNG)
![del](img/eliminar.PNG)

# Autor

Sebastian Saldarriaga Muñoz  
LinkedIn: [@sebastian-saldarriaga-muñoz](https://www.linkedin.com/in/sebastian-saldarriaga-muñoz)
